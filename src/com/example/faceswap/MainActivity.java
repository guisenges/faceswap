package com.example.faceswap;

/*
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
*/

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

/*
import android.content.res.Resources;
import android.util.Log;
*/

/*
import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.objdetect.CascadeClassifier;
*/

public class MainActivity extends Activity {

	/*
	public static CascadeClassifier faceDetector;
	private static File mCascadeFile;
	*/
	public static final String TAG = "FaceSwap";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	    /*
	    Log.i(TAG, "Trying to load OpenCV library");
	    if (!OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_2, this, mOpenCVCallBack))
	    {
	      Log.e(TAG, "Cannot connect to OpenCV Manager");
	    }
	    */
	    
	    //Remove title
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        //Change to full screen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
	}
	
	/*
	private BaseLoaderCallback mOpenCVCallBack = new BaseLoaderCallback(this) {
		@Override
		public void onManagerConnected(int status) {
		   switch (status) {
		       case LoaderCallbackInterface.SUCCESS:
		       {
		      Log.i(TAG, "OpenCV loaded successfully");
		      // Create and set View
		      MainActivity.loadCascadeFile(getResources(), getDir("cascade", Context.MODE_PRIVATE));
		      setContentView(R.layout.activity_main);
		       } break;
		       default:
		       {
		    	   Log.i(TAG, "Error loading OpenCV");
		       } break;
		   }
	    }
	};
		
	public static void loadCascadeFile(Resources res, File cascadeDir) {
		try {
            // load cascade file from application resources
            InputStream is = res.openRawResource(R.raw.haarcascade_frontalface_alt);
            mCascadeFile = new File(cascadeDir, "haarcascade_frontalface_alt.xml");
            FileOutputStream os = new FileOutputStream(mCascadeFile);

            byte[] buffer = new byte[4096];
            int bytesRead;
            while ((bytesRead = is.read(buffer)) != -1) {
                os.write(buffer, 0, bytesRead);
            }
            is.close();
            os.close();

            faceDetector = new CascadeClassifier(mCascadeFile.getAbsolutePath());
            if (faceDetector.empty()) {
                Log.e(TAG, "Failed to load cascade classifier");
                faceDetector = null;
            } else
                Log.i(TAG, "Loaded cascade classifier from " + mCascadeFile.getAbsolutePath());

            cascadeDir.delete();

        } catch (IOException e) {
            e.printStackTrace();
            Log.e(TAG, "Failed to load cascade. Exception thrown: " + e);
        }
	}
	*/
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	public void takePicture(View view) {
		if (checkCameraHardware(this)) {
			Intent intent = new Intent(this, CameraPreviewActivity.class);
	        startActivity(intent);
		} 
		else {
			Toast.makeText(this, getString(R.string.no_camera), Toast.LENGTH_LONG).show();
		}
	}
	
	/** Check if this device has a camera */
	private boolean checkCameraHardware(Context context) {
	    if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
	        // this device has a camera
	        return true;
	    } else {
	        // no camera on this device
	        return false;
	    }
	}
}
