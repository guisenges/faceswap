package com.example.faceswap;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

public class SwapPreviewActivity extends Activity {
	
	public static final String TAG = "FaceSwap";
	private Uri imagePath;
	private byte[] data;
	private byte[] new_data;
	private ImageView image;
	private Bitmap bmp;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);		
		Log.i(TAG, "Started Swap Preview Activity");
		
		//Remove title
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        //Change to full screen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_swap_preview);
		
		// Show the Up button in the action bar.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			setupActionBar();
		}
		
		Log.i(TAG, "Getting photo byte array...");
		//Get image name from URI
		imagePath = getIntent().getData();
		//Get bitmap from image file
		Bitmap captureBmp = BitmapFactory.decodeFile(imagePath.toString());
		//Convert back to byte array
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		captureBmp.compress(Bitmap.CompressFormat.JPEG, 100, stream);
		data = stream.toByteArray();
		
		Log.i(TAG, "Begin swap procedure...");
		//new_data = SwapProcedure.swap(data, MainActivity.faceDetector);
		new_data = SwapProcedure.swap(data);
		setImage(new_data);
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig){
		super.onConfigurationChanged(newConfig);
		
		resizeImage(newConfig);
	}
	
	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.swap_preview, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private void setImage(byte[] data) {
		Log.i(TAG, "Starting to set image size...");
		bmp = BitmapFactory.decodeByteArray(data, 0, data.length);
		
		image = (ImageView) findViewById(R.id.swap_preview_photo);
		Configuration conf = getResources().getConfiguration();
		resizeImage(conf);
	}
	
	public void discardImage(View view) {
		
		//Delete image
		File file = new File(imagePath.toString());
	    if(file.exists()) {
	         file.delete();
	    }
	    
		NavUtils.navigateUpFromSameTask(this);
	}
	
	public void saveImage(View view) throws IOException {
		saveFile(new_data, "SWAPPED");
		Intent intent = new Intent(this, MainActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
	}
	
	private void resizeImage(Configuration newConfig) {
		double bmp_width = bmp.getWidth();
		double bmp_height = bmp.getHeight();
		double bmpAspectRatio = (bmp_width / bmp_height);
		
		DisplayMetrics metrics = getResources().getDisplayMetrics();
		
		int width = 0;
		int height = 0;
		
		int actionHeight = 0;
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			TypedValue tv = new TypedValue();
			if (getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
				actionHeight = (int) TypedValue.complexToDimension(tv.data, getResources().getDisplayMetrics());
			}
		}
		
		int maxHeight = metrics.heightPixels - actionHeight;
		
		if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE){
			height = maxHeight;
			width = (int) (height * bmpAspectRatio);
		} else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
			width = metrics.widthPixels;
			height = (int) (width / bmpAspectRatio);
		}
		
		image.setImageBitmap(Bitmap.createScaledBitmap(bmp, width, height, false));
	}
	
	

	/**
	 * SAVING MEDIA FILES
	 * @throws IOException 
	 */
	
	private void saveFile(byte[] data, String extra) throws IOException {
		//Save image to SD card
		Bitmap picture = BitmapFactory.decodeByteArray(data, 0, data.length);
        try {
            FileOutputStream out = new FileOutputStream(imagePath.toString().replaceAll(".jpg", extra).concat(".jpg"));
            picture.compress(Bitmap.CompressFormat.JPEG, 100, out);
            picture.recycle();
        } catch (Exception e) {
            e.printStackTrace();
        }
	}
}
