package com.example.faceswap;

public class Point {
	private final Integer x;
	private final Integer y;

	public Point( Integer x, Integer y ) {
		this.x = x;
		this.y = y;
	}

	public Integer getX() {
		return this.x;
	}
	public Integer getY() {
		return this.y;
	}

	@Override
	public boolean equals( Object oth ) {
		if ( this == oth ) {
			return true;
		}
		if ( oth == null || !(this.getClass().isInstance( oth )) ) {
			return false;
		}
		
		Point other = this.getClass().cast( oth );
		
		return (this.x == null? other.x == null : this.x.equals( other.x ))
				&& (this.y == null? other.y == null : this.y.equals( other.y ));
	}

}