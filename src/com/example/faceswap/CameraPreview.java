package com.example.faceswap;

import java.io.IOException;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.hardware.Camera;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

@SuppressLint({ "NewApi", "ViewConstructor" })
public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback {
	
	private SurfaceHolder mHolder;
    private Camera mCamera;

	@SuppressWarnings("deprecation")
	public CameraPreview(Context context, Camera camera) {
		super(context);
		mCamera = camera;

        // Install a SurfaceHolder.Callback so we get notified when the
        // underlying surface is created and destroyed.
        mHolder = getHolder();
        mHolder.addCallback(this);
        
        // deprecated setting, but required on Android versions prior to 3.0
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
        	mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        }
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
		// If your preview can change or rotate, take care of those events here.
        // Make sure to stop the preview before resizing or reformatting it.

        if (mHolder.getSurface() == null){
          // preview surface does not exist
          return;
        }

        // stop preview before making changes
        try {
            mCamera.stopPreview();
        } catch (Exception e){
          // ignore: tried to stop a non-existent preview
        }

        // set preview size and make any resize, rotate or
        // reformatting changes here
        Camera.Parameters params = mCamera.getParameters();
        Camera.Size bestSize = getBestSize(width, height, params);
        
        if (bestSize != null) {
			params.setPreviewSize(bestSize.width, bestSize.height);
			mCamera.setParameters(params);
			
			FrameLayout layout = (FrameLayout) this.getParent();
			
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
				//API >= 11
				layout.setX((getContext().getResources().getDisplayMetrics().widthPixels - (bestSize.width * getMaxHeight() / bestSize.height))/2);
			}
			else {
				//Older versions
				layout.setLayoutParams(new RelativeLayout.LayoutParams(bestSize.width * getMaxHeight() / bestSize.height, getMaxHeight()));
			}
			
			/*Toast.makeText(getContext(),"MaxHeight: " + String.valueOf(getMaxHeight()) + 
					"\nWidth: " + String.valueOf(bestSize.width) + 
					"\nHeight: " + String.valueOf(bestSize.height), Toast.LENGTH_LONG).show();*/
		}

        // start preview with new settings
        try {
            mCamera.setPreviewDisplay(mHolder);
            mCamera.startPreview();

        } catch (Exception e){
            Log.d(VIEW_LOG_TAG, "Error starting camera preview: " + e.getMessage());
        }
	}

	private Camera.Size getBestSize(int width, int height, Camera.Parameters params) {
		Camera.Size bestSize = null;
		List<Camera.Size> sizeList = params.getSupportedPreviewSizes();
		
		int currentWidth = 0;
		int currentHeight = 0;
		
		for(int i = 0; i<sizeList.size(); i++){
			if(sizeList.get(i).height < getMaxHeight()) {
				if((sizeList.get(i).width * sizeList.get(i).height) > (currentWidth * currentHeight)){
					bestSize = sizeList.get(i);
					currentWidth = bestSize.width;
					currentHeight = bestSize.height;
				}
			}
		}
		
		return bestSize;
	}
	
	private int getMaxHeight() {
		DisplayMetrics metrics = getContext().getResources().getDisplayMetrics();
		int actionHeight = 0;
		
		TypedValue tv = new TypedValue();
		if (getContext().getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
			actionHeight = (int) TypedValue.complexToDimension(tv.data, getContext().getResources().getDisplayMetrics());
		}
		
		return metrics.heightPixels - actionHeight;
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		// The Surface has been created, now tell the camera where to draw the preview.
        try {
            mCamera.setPreviewDisplay(holder);
            mCamera.startPreview();
        } catch (IOException e) {
            Log.d("CAMERA", "Error setting camera preview: " + e.getMessage());
        }
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		
	}

}
