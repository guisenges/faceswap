package com.example.faceswap;

import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;

@SuppressLint("SimpleDateFormat")
public class CameraPreviewActivity extends Activity {
	
	public static final String TAG = "FaceSwap";
	protected static final String PHOTO_DATA = "com.example.faceswap.PHOTO_DATA";
	private Camera mCamera;
	private CameraPreview mSurfacePreview;
	private FrameLayout prev;
	private Intent swapPreviewIntent;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		//Remove title
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        //Change to full screen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_camera_preview);
		
		// Show the Up button in the action bar.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			setupActionBar();
		}
	
        prev = (FrameLayout) findViewById(R.id.camera_view);
        startCamera();
	}
	
	private void startCamera() {
		if (mCamera == null){
			mCamera = getCameraInstance();
			mSurfacePreview = new CameraPreview(this, mCamera);
			prev.addView(mSurfacePreview);
		}
	}
	
	/** A safe way to get an instance of the Camera object. */
	public static Camera getCameraInstance(){
	    Camera c = null;
	    try {
	        c = Camera.open(); // attempt to get a Camera instance
	    }
	    catch (Exception e){
	        // Camera is not available (in use or does not exist)
	    }
	    return c; // returns null if camera is unavailable
	}
	
	public void capturePic(View view){
		swapPreviewIntent = new Intent(CameraPreviewActivity.this, SwapPreviewActivity.class);
		
		PictureCallback mPicture = new PictureCallback() {
			
			@Override
			public void onPictureTaken(byte[] data, Camera camera) {
				Log.i(TAG, "Picture taken");		
				
				//Set pictura name
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
		        String currentDateandTime = sdf.format(new Date());
		        String mPictureFileName = Environment.getExternalStorageDirectory().getPath() +
		                               "/sample_picture_" + currentDateandTime + ".jpg";
				
		        //Save image to SD card
				Bitmap picture = BitmapFactory.decodeByteArray(data, 0, data.length);
                try {
                    FileOutputStream out = new FileOutputStream(mPictureFileName);
                    picture.compress(Bitmap.CompressFormat.JPEG, 100, out);
                    picture.recycle();
                    mCamera.startPreview();
                } catch (Exception e) {
                    e.printStackTrace();
                }
				
                //Send image name to next intent
				swapPreviewIntent.setData(Uri.parse(mPictureFileName));
				//Start next intent
				startActivity(swapPreviewIntent);
			}
		};
		
		mCamera.takePicture(null, null, mPicture);
	}

	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.camera_preview, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	protected void onResume(){
		super.onResume();
		startCamera();
	}
	
	@Override
	protected void onPause(){
		super.onPause();
		releaseCamera();
	}

	private void releaseCamera() {
		if (mCamera != null){
			mCamera.setPreviewCallback(null);
	        mSurfacePreview.getHolder().removeCallback(mSurfacePreview);
            mCamera.release();        // release the camera for other applications
            mCamera = null;
            prev.removeView(mSurfacePreview);
        }
	}
}
