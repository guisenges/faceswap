package com.example.faceswap;

import java.io.ByteArrayOutputStream;
import java.util.Arrays;
import java.util.EmptyStackException;
import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.util.Log;
import com.example.faceswap.Point;


public class SwapProcedure {

	public static final String TAG = "FaceSwap";
	private static final double desiredNumPixels = 1200000; // 1.2MP
	
	public static byte[] swap(byte[] data) {
		Bitmap bmp = BitmapFactory.decodeByteArray(data, 0, data.length, new BitmapFactory.Options());

		//Skin tone filtering
		Bitmap bmpYCbCr = skintoneFilter(bmp);

		return compressToJPEG_andGetByteArray(bmpYCbCr);
	}

	/*
	private static void openCVDetectProcedure(Bitmap bmp, CascadeClassifier faceDetector) {
		Mat coloredMat = new Mat(bmp.getWidth(), bmp.getHeight(), CvType.CV_8UC4);
		Mat grayMat = new Mat(bmp.getWidth(), bmp.getHeight(), CvType.CV_8UC1);
		Utils.bitmapToMat(bmp, coloredMat); 
		Imgproc.cvtColor(coloredMat, grayMat, Imgproc.COLOR_RGBA2GRAY);		
		Imgproc.equalizeHist(grayMat, grayMat);
	
		MatOfRect faces = new MatOfRect();
	
		Size minSize = new Size(bmp.getWidth()/12, bmp.getHeight()/12);
		Size maxSize = new Size(bmp.getWidth()/2, bmp.getHeight()/2);
		faceDetector.detectMultiScale(grayMat, faces, 1.1, 3, 0, minSize, maxSize);
	
		List<Rect> list = faces.toList();

		if (list.size() >= 2)
		{
			Rect first = list.get(0);
			Rect second = list.get(1);
			int avgWidth = (first.width + second.width)/2;
			int avgHeight = (first.height + second.height)/2;
	
			int sourcePoint1x = first.x + (first.width - avgWidth)/2;
			int sourcePoint1y = first.y + (first.height - avgHeight)/2;
	
			int centralPoint1x = sourcePoint1x + avgWidth/2;
			int centralPoint1y = sourcePoint1y + avgHeight/2;
	
			int sourcePoint2x = second.x + (second.width - avgWidth)/2;
			int sourcePoint2y = second.y + (second.height - avgHeight)/2;
	
			//Point pt1top = new Point(sourcePoint1x, sourcePoint1y);
			//Point pt1bot = new Point(sourcePoint1x + avgWidth, sourcePoint1y + avgHeight);
	        //Core.rectangle(coloredMat, pt1top, pt1bot, new Scalar( 0, 255, 0, 0 ), 5, 8, 0);
	
			//Point pt2top = new Point(sourcePoint2x, sourcePoint2y);
			//Point pt2bot = new Point(sourcePoint2x + avgWidth, sourcePoint2y + avgHeight);
	        //Core.rectangle(coloredMat, pt2top, pt2bot, new Scalar( 0, 255, 0, 0 ), 5, 8, 0);
	
			float maxTolerance = 0.35f * avgWidth;
			double[] dummyVector;
	
			for( int x = 0 ; x < avgWidth ; x++){
				for( int y = 0 ; y < avgHeight ; y++){
					if (pointRadius(x + sourcePoint1x, y + sourcePoint1y, centralPoint1x, centralPoint1y) < maxTolerance) {
						dummyVector = coloredMat.get(sourcePoint2y + y, sourcePoint2x + x);
						coloredMat.put(sourcePoint2y + y, sourcePoint2x + x, coloredMat.get(sourcePoint1y + y, sourcePoint1x + x));
						coloredMat.put(sourcePoint1y + y, sourcePoint1x + x, dummyVector);
					}
				}
			}
		}

		Utils.matToBitmap(coloredMat, bmp);
	}
	*/
	
	private static Bitmap skintoneFilter(Bitmap sourceBmp) {
		Bitmap bmpYCbCr = sourceBmp.copy(sourceBmp.getConfig(), true);
		int pixelColor, red, green, blue;
		float cb, cr; //y
		
		for (int i = 0; i<bmpYCbCr.getWidth(); i++) {
			for (int j = 0; j<bmpYCbCr.getHeight(); j++) {
				pixelColor = bmpYCbCr.getPixel(i, j);
				red = Color.red(pixelColor);
				green = Color.green(pixelColor);
				blue = Color.blue(pixelColor);

				//y = 0.299f * red + 0.587f * green + 0.114f * blue;
				cb = -0.169f * red + -0.332f * green + 0.5f * blue;
				cr = 0.5f * red - 0.419f * green + 0.081f * blue;

				if ( (-32 < cb) && ( cb < 0 ) && ( 14 < cr ) && ( cr < 32 ) ){
					bmpYCbCr.setPixel(i, j, Color.WHITE);
				}
				else {
					bmpYCbCr.setPixel(i, j, Color.BLACK);
				}

			}
		}
		
		return bmpYCbCr;
	}
	
	private static Bitmap sobelFilter(Bitmap sourceBmp) {
		Bitmap sobelBmp = sourceBmp.copy(Bitmap.Config.ARGB_8888, true);
		
        Mat in = new Mat(); 
        Utils.bitmapToMat(sobelBmp, in);
        
        if( ! in.empty() ) {
        	Mat out = new Mat(); 
        	
        	Imgproc.Sobel(in, out, -1 /* means the same as src.depth() */, 1, 0);
        	
        	Utils.matToBitmap(out, sobelBmp);
        	
        	if( out.empty() ) {
        		Log.e(TAG, "Can't convert 'out' to Mat");
        	} 
        } 
        else {
        	Log.e(TAG, "Can't convert 'src32' to Mat");
        }
		
		return sobelBmp;
	}
	
	private static Bitmap cannyFilter(Bitmap sourceBmp) {
		Bitmap cannyBmp = sourceBmp.copy(sourceBmp.getConfig(), true);
		
		Mat in = new Mat();
		Mat out = new Mat(); 
        Utils.bitmapToMat(cannyBmp, in);
        
        if( ! in.empty() ) {
        	Imgproc.Canny(in, out, 3, 3);
        	
        	Utils.matToBitmap(out, cannyBmp);
        }
		
		return cannyBmp;
	}
	
	/* Metodo generico para eliminar pequenos ruidos de pixels
	 * pretos nas areas brancas e vice-versa */
	private static Bitmap morfologicTypeFilter(Bitmap sourceBmp, String morfologicType) {
		Bitmap processedBmp = sourceBmp.copy(sourceBmp.getConfig(), true);
		
		Mat in = new Mat();
		Mat out = new Mat();
        Utils.bitmapToMat(processedBmp, in);
        
        out = Mat.zeros(in.rows(), in.cols(), in.depth());
        
        if( ! in.empty() ) {
        	//Nao pegar pixels da borda
        	for(int r = 1; r < (in.rows()-1); r++) {
        		for(int c = 1; c < (in.cols()-1); c++) {
        			
        			double[][] neigh = new double[9][in.depth()];
        			int index = 0;
        			
        			//Iterar sobre todos os vizinhos na regiao 3x3
        			//Filtro considera todos os pixels vizinhos da região
        			for(int nr = -1; nr <= 1; nr++) {
        				for(int nc = -1; nc <= 1; nc++) {
        					
        					neigh[index] = in.get( (r+nr), (c+nc) );
        					index++;
        				}
        			}
            		
        			//Ordenar e pegar o valor que representa o filtro desejado
        			Arrays.sort(neigh);
        			if(morfologicType.equals("median")) {
        				out.put(r, c, neigh[4]);
        			}
        			else if(morfologicType.equals("dilation")) {
        				out.put(r, c, neigh[8]);
        			}
        			else if(morfologicType.equals("erosion")) {
        				out.put(r, c, neigh[0]);
        			}
        			else {
        				throw new EmptyStackException();
        			}
            	}
        	}
        }
        
        Utils.matToBitmap(out, processedBmp);
		
		return processedBmp;
	}

	
	private static Bitmap medianFilter(Bitmap sourceBmp) {
		Bitmap medianBmp = morfologicTypeFilter(sourceBmp, "median");
		return medianBmp;
	}
	
	private static Bitmap erosionFilter(Bitmap sourceBmp) {
		Bitmap erosionBmp = morfologicTypeFilter(sourceBmp, "erosion");
		return erosionBmp;
	}
	
	private static Bitmap dilationFilter(Bitmap sourceBmp) {
		Bitmap dilationBmp = morfologicTypeFilter(sourceBmp, "dilation");
		return dilationBmp;	
	}
	
	/* Verificar se area e aspecto da regiao conexa sao
	 * plausiveis para um possivel rosto */
	private static <T,U> boolean isAValidAspectArea(
			Mat region, Point min, Point max, 
			Integer maxExpectedArea, Integer minExpectedArea) {
		
		Integer boxWidth = max.getX() - min.getX();
		Integer boxHeight = max.getY() - min.getY();
		
		//Verificar aspecto
		Integer ratio = (boxWidth) * (boxHeight);
		if(!(ratio > 0.6 && ratio < 1.5)) {
			return false;
		}
		
		//Verificar area
		Integer regionPixels = 0;
		for(int x = 0; x < boxWidth; x++) {
			for(int y = 0; y < boxHeight; y++) {
				if(region.get(x, y).equals(1)) {
					regionPixels++;
				}
			}
		}
		if(!(minExpectedArea < regionPixels && regionPixels < maxExpectedArea)) {
			return false;
		}
		
		return true;
	}
	
	private static int pointRadius(int x1, int y1, int x2, int y2) {
		int xSquaredDifference = ( x1 - x2 ) * ( x1 - x2 );
		int ySquaredDifference = ( y1 - y2 ) * ( y1 - y2 );
		return Double.valueOf(Math.sqrt(xSquaredDifference + ySquaredDifference)).intValue(); 
	}

	private static Bitmap scaleAndGetBitmap(byte[] data) {
		BitmapFactory.Options bmpOptions = new BitmapFactory.Options();
		bmpOptions.inJustDecodeBounds = true;
		BitmapFactory.decodeByteArray(data, 0, data.length, bmpOptions);

		double currentNumPixels = bmpOptions.outHeight * bmpOptions.outWidth;
		double quadraticFactory = (currentNumPixels/desiredNumPixels);
		quadraticFactory += quadraticFactory / 100;
		int scaleFactor = (int) Math.ceil(Math.sqrt(quadraticFactory));	

		bmpOptions.inJustDecodeBounds = false;
		bmpOptions.inSampleSize = scaleFactor;

		Bitmap scaledBmp = BitmapFactory.decodeByteArray(data, 0, data.length, bmpOptions);

		return scaledBmp;
	}

	private static byte[] compressToJPEG_andGetByteArray(Bitmap finalBmp) {
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		finalBmp.compress(Bitmap.CompressFormat.JPEG, 100, stream);
		return stream.toByteArray();
	}

}
